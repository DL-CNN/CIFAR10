import tensorflow as tf
from packages.mymodels.base import weight_variable, bias_variable
from packages.mymodels.base import conv2d, max_pool_2x2, batch_normalize



def graph(x, keep_prob):
    with tf.name_scope("ReshapeInput"):
        x_image = tf.reshape(x, [-1,3,32,32])
        x_image = tf.transpose(x_image, perm=[0,2,3,1])
    with tf.name_scope('Layer1'):
        W_conv1 = weight_variable([5, 5, 3, 32])
        b_conv1 = bias_variable([32])
        h_conv1 = conv2d(x_image, W_conv1) + b_conv1
        h_conv1 = batch_normalize(h_conv1)
        h_conv1 = tf.nn.relu(h_conv1)
        h_pool1 = max_pool_2x2(h_conv1)
    
    with tf.name_scope('Layer2'):
        W_conv2 = weight_variable([5, 5, 32, 64])
        b_conv2 = bias_variable([64])
        h_conv2 = conv2d(h_pool1, W_conv2) + b_conv2
        h_conv2 = batch_normalize(h_conv2)
        h_conv2 = tf.nn.relu(h_conv2)
        h_pool2 = max_pool_2x2(h_conv2)

    with tf.name_scope('Layer3'):
        W_conv3 = weight_variable([5, 5, 64, 128])
        b_conv3 = bias_variable([128])
        h_conv3 = conv2d(h_pool2, W_conv3) + b_conv3
        h_conv3 = batch_normalize(h_conv3)
        h_conv3 = tf.nn.relu(h_conv3)
        h_pool3 = max_pool_2x2(h_conv3)
    
    with tf.name_scope('DenselyConnested'):
        W_fc1 = weight_variable([4 * 4 * 128, 1024])
        b_fc1 = bias_variable([1024])
        h_pool3_flat = tf.reshape(h_pool3, [-1, 4*4*128])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool3_flat, W_fc1) + b_fc1)

        """W_fc1b = weight_variable([1024, 1024])
        b_fc1b = bias_variable([1024])
        h_fc1b = tf.nn.relu(tf.matmul(h_fc1a, W_fc1b) + b_fc1b)

        W_fc1c = weight_variable([1024, 1024])
        b_fc1c = bias_variable([1024])
        h_fc1 = tf.nn.relu(tf.matmul(h_fc1b, W_fc1c) + b_fc1c)"""
        
    with tf.name_scope('Dropout'):
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
        
    with tf.name_scope('Readout'):
        W_fc2 = weight_variable([1024, 10])
        b_fc2 = bias_variable([10])
        y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

    tf.summary.image('summary_input', (x_image[:,:,:,0:3]))
    tf.summary.image('summary_hpool1', (h_pool1[:,:,:,0:3]))
    tf.summary.image('summary_hpool2', (h_pool2[:,:,:,0:3]))
    tf.summary.image('summary_hpool3', (h_pool3[:,:,:,0:3]))
    tf.summary.histogram("input", x_image)
    tf.summary.histogram("h_conv1", h_conv1)
    tf.summary.histogram("h_conv2", h_conv2)
    tf.summary.histogram("h_conv3", h_conv3)
    tf.summary.histogram("h_fc1", h_fc1)
    tf.summary.histogram("h_fc1_drop", h_fc1_drop)
    tf.summary.histogram("y_conv", y_conv)
    
    return y_conv
