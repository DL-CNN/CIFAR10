import tensorflow as tf
from packages.mymodels.base import weight_variable, bias_variable
from packages.mymodels.base import conv2d, max_pool_2x2, batch_normalize

""" The All-CNN model
as given in Striving for Simplicity: The All Convolutional Net, ICLR 2015

BASE MODEL C is implemented (other improvements like Conv pool can be added later)

"""

def graph(x, keep_prob):
    with tf.name_scope("ReshapeInput"):
        x_image = tf.reshape(x, [-1,3,32,32])
        x_image = tf.transpose(x_image, perm=[0,2,3,1])
    with tf.name_scope('Layer1'):
        W_conv1 = weight_variable([3, 3, 3, 96])
        b_conv1 = bias_variable([96])
        h_conv1 = conv2d(x_image, W_conv1) + b_conv1
        h_conv1 = batch_normalize(h_conv1)
        h_conv1 = tf.nn.relu(h_conv1)

        W_conv1b = weight_variable([3, 3, 96, 96])
        b_conv1b = bias_variable([96])
        h_conv1b = conv2d(h_conv1, W_conv1b) + b_conv1b
        h_conv1b = batch_normalize(h_conv1b)
        h_conv1b = tf.nn.relu(h_conv1b)

        h_pool1 = max_pool_2x2(h_conv1b)
    
    with tf.name_scope('Layer2'):
        W_conv2 = weight_variable([3, 3, 96, 192])
        b_conv2 = bias_variable([192])
        h_conv2 = conv2d(h_pool1, W_conv2) + b_conv2
        h_conv2 = batch_normalize(h_conv2)
        h_conv2 = tf.nn.relu(h_conv2)

        W_conv2b = weight_variable([3, 3, 192, 192])
        b_conv2b = bias_variable([192])
        h_conv2b = conv2d(h_conv2, W_conv2b) + b_conv2b
        h_conv2b = batch_normalize(h_conv2b)
        h_conv2b = tf.nn.relu(h_conv2b)

        h_pool2 = max_pool_2x2(h_conv2b)

    with tf.name_scope('Layer3'):
        W_conv3 = weight_variable([3, 3, 192, 192])
        b_conv3 = bias_variable([192])
        h_conv3 = conv2d(h_pool2, W_conv3) + b_conv3
        h_conv3 = batch_normalize(h_conv3)
        h_conv3 = tf.nn.relu(h_conv3)

        W_conv3b = weight_variable([1, 1, 192, 192])
        b_conv3b = bias_variable([192])
        h_conv3b = conv2d(h_conv3, W_conv3b) + b_conv3b
        h_conv3b = batch_normalize(h_conv3b)
        h_conv3b = tf.nn.relu(h_conv3b)

        W_conv3c = weight_variable([1, 1, 192, 10])
        b_conv3c = bias_variable([10])
        h_conv3c = conv2d(h_conv3b, W_conv3c) + b_conv3c
        h_conv3c = batch_normalize(h_conv3c)
        h_conv3c = tf.nn.relu(h_conv3c)
        #h_pool3 = max_pool_2x2(h_conv3c)
    
    with tf.name_scope('Final'):
        y_conv = tf.reduce_mean(h_conv3c,1) #average along x-axis
        y_conv = tf.reduce_mean(y_conv,1) #average along y-axis

    tf.summary.image('summary_input', (x_image[:,:,:,0:3]))
    tf.summary.image('summary_hpool1', (h_pool1[:,:,:,0:3]))
    tf.summary.image('summary_hpool2', (h_pool2[:,:,:,0:3]))
    #tf.summary.image('summary_hpool3', (h_pool3[:,:,:,0:3]))
    tf.summary.histogram("input", x_image)
    tf.summary.histogram("h_conv1", h_conv1)
    tf.summary.histogram("h_conv2", h_conv2)
    tf.summary.histogram("h_conv3", h_conv3)
    #tf.summary.histogram("h_fc1", h_fc1)
    #tf.summary.histogram("h_fc1_drop", h_fc1_drop)
    tf.summary.histogram("y_conv", y_conv)
    
    return y_conv
