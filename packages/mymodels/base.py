import tensorflow as tf


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1], padding='SAME')

def batch_normalize(input, axes = [0,1,2], norm_param = {'offset':0, 'scale':1, 'variance_epsilon':1e-5}):
    """normalize input data
    Parameters
    ----------
    input : tensor
    axes : scalar or list
        to calculate moments along the axes
    norm_param : dict
        parameters for normalization
    """
    mean, var = tf.nn.moments(input, axes = axes)
    offset = norm_param['offset']
    scale = norm_param['scale']
    variance_epsilon = norm_param['variance_epsilon']
    output = tf.nn.batch_normalization(input, mean, var, offset, scale, variance_epsilon)
    return output
