# CIFAR10

### Description
["The CIFAR-10 dataset consists of 60000 32x32 colour images in 10 classes, with 6000 images per class. There are 50000 training images and 10000 test images. 
The dataset is divided into five training batches and one test batch, each with 10000 images. 
The test batch contains exactly 1000 randomly-selected images from each class. 
The training batches contain the remaining images in random order, but some training batches may contain more images from one class than another. 
Between them, the training batches contain exactly 5000 images from each class.".](https://www.cs.toronto.edu/~kriz/cifar.html)

The data is available in two formats, one contains full numbers with variable resolution, whereas the other contains, cropped numbers with fixed resolution.
For this work, the data with cropped numbers with fixed resolution have been used, as shown in the image below.

![alt text](https://i2.wp.com/www.aimechanic.com/wp-content/uploads/2016/09/Screen-Shot-2016-09-30-at-10.00.01.png?resize=467%2C363)

### Index
* [Structure of the repository](https://gitlab.com/DL-CNN/CIFAR10#structure-of-the-repository)
* [Results](https://gitlab.com/DL-CNN/CIFAR10#implementation-results)
* [Conclusion](https://gitlab.com/DL-CNN/CIFAR10#conclusion)


### Elaboration

#### Structure of the repository
* packages: contains module definitions created for this work
* notebooks: contain jupyter notebooks
* scripts: .py scripts for training and evaluations
  * /logsTf: contains the tensorboard graph files, arranged as '/../train' and '/../test' directories
* TrainedNets: contains the log files for various evaluations
* dataset: contains the data files used for this work

#### Implementation & Results
* Training model
  The training model is an adaption the All CNN model proposed in the work by [Springenberg et al](https://arxiv.org/abs/1412.6806)
and the respective tensorboard visualization is given in the image below.

![alt text](/misc/model.png)
* Test Sample with a feature activation

![alt text](/misc/test_sample.png)
![alt text](/misc/feature1_activation.png)
* Overall accuracy

Training time ~10.5hrs

![alt text](/misc/overall_accuracy.png)
* Overall cross entropy loss

![alt text](/misc/overall_crossentropy_loss.png)

#### Conclusion
This work was aimed at developing our skills on classification based tasks on image data using CNN.
For this task an adaptation of the model named as [All CNN](https://arxiv.org/abs/1412.6806) has been implemented.
An important step is the use of batch normalization of each feature between every convolution and ReLU activations, more aptly refering to 'Feature Normalization'.

Another aim of this work is to use the same training architecture for multiple tasks (as has also been done for our [DLL-CNN:SVHN](https://gitlab.com/DL-CNN/SVHN) project)
and make sustainable improvements in the architecture (meaning, improvements that are relevant globally and not restricted to a specific problem).
In that respect, a comprehensive evaluation shall be offered as a valid conclusion of this work.


NOTE: set `pid=<string>` in train.py before performing evaluations, to store as the sub-directory in 'TrainedNets/' and 'scripts/logsTf/'
