#!/bin/bash

#SBATCH --nodes 1
#SBATCH -p hsw_p100
#SBATCH --job-name DL
#SBATCH --time=24:00:00

module purge
module load PrgEnv/PGI+OpenMPI/2017-03-18
module load cuda/8.0.44  
module load cudnn/cuda80-v51

/home/ksrivastava/keras/bin/python /home/ksrivastava/DL/CIFAR10/setup.py develop
/home/ksrivastava/keras/bin/python /home/ksrivastava/DL/CIFAR10/scripts/trainGPU.py
