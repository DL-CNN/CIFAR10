import numpy as np
from packages.mymodels import CNN3L, CNN9L, allCNN
from packages import misc
from sklearn.model_selection import train_test_split
import tensorflow as tf
import os
import shutil
import time


start = time.time()

pid = 'all_cnn_long'+'_GPU' #Temporary <directory name> for storing log files


# ---------------------Making sure that only one GPU is visible-------------------------
import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from tensorflow.python.client import device_lib

#------------------------Defining function for pickle-----------------------------------------
'''
def unpickle(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict
'''

filename = "/home/ksrivastava/DL/CIFAR10/dataset/cifar-10-batches-py/data_batch_"
"""temp = misc.unpickle(filename)
data = temp[b'data']
labels = misc.dense_to_one_hot(np.array(temp[b'labels']), 10)"""
#----CIFAR10 data acquisition----
data1, label1 = misc.get_CIFAR_data(filename+"1")
data2, label2 = misc.get_CIFAR_data(filename+"2")
data3, label3 = misc.get_CIFAR_data(filename+"3")
data4, label4 = misc.get_CIFAR_data(filename+"4")
data5, label5 = misc.get_CIFAR_data(filename+"5")
data = np.concatenate((data1,data2,data3,data4,data5))
labels = np.concatenate((label1,label2,label3,label4,label5))
print('Sample data and label: ', len(data), data[0].shape, labels[0])

#----Train test split---
test_size = 0.3
random_state = 42
training_data, validation_data, training_label, validation_label = train_test_split(data, labels, test_size=test_size, random_state=random_state)

print('Training data: ', np.shape(training_data), np.shape(training_label),'\nValidation data: ', np.shape(validation_data),np.shape(validation_label))

"""
#----Creating a list whose 1st element consists of two parts: combination of data and label-----
list1 = list()
for i in range(len(data)):
    temp = list()
    temp.append(data[i])
    temp.append(labels[i])
    list1.append(temp)
#----dividing the data into training , validation and test data ----------------------------
training = list1[0:6000]
validation = list1[6000:8000]
test = list1[8000:10000]
#----rearranging the data again phew ! -------------------------------------------------------
training_data = list()
training_label = list()
validation_data = list()
validation_label = list()
test_data = list()
test_label = list()

for i in training:
    training_data.append(i[0])
    training_label.append(i[1])

for j in validation:
    validation_data.append(j[0])
    validation_label.append(j[1])

for a in test:
    test_data.append(a[0])
    test_label.append(a[1])


validation_data = np.asarray(validation_data)
validation_label = np.asarray(validation_label)

test_data = np.asarray(test_data)
test_label = np.asarray(test_label)

print('Training data: ', np.shape(training_data), np.shape(training_label),'\nValidation data: ', np.shape(validation_data),np.shape(validation_label), '\nTest data: ', np.shape(test_data),np.shape(test_label))
"""

x = tf.placeholder(tf.float32, [None, 3072])
keep_prob = tf.placeholder(tf.float32)
y_ = tf.placeholder(tf.float32, [None, 10])

y_conv = allCNN.graph(x,keep_prob)
y_conv = tf.nn.softmax(y_conv)

cross_entropy = tf.reduce_mean(-tf.reduce_sum((y_ * tf.log(tf.clip_by_value(y_conv, 1e-15, 1))), reduction_indices=[-1]))
#cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
tf.summary.scalar("cross_entropy", cross_entropy)
train_step = tf.train.AdamOptimizer(1e-3).minimize(cross_entropy)
correct_prediction = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
tf.summary.scalar("accuracy", accuracy)

batch_size = 64
n_examples = len(training_data)
n_batches = int(n_examples / batch_size)

#------------------------------Training CNN -----------------------------------------------
#file=open("Adam_3conv_layer_output.txt","w")
#file.write("Loss\t")
#file.write("Accuracy\n")
init = tf.global_variables_initializer()
sess = tf.Session()

#Summary for tensorboard-------------------------------------------------
merge_summary = tf.summary.merge_all()
if os.path.isdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid):
    shutil.rmtree('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid)
if not os.path.isdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid):
    os.mkdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid)
    if not os.path.isdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid+'/train'):
        os.mkdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid+'/train')
    if not os.path.isdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid+'/validation'):
        os.mkdir('/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid+'/validation')
summary_training = tf.summary.FileWriter(os.path.join(os.getcwd(),'/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid+'/train'), sess.graph)
summary_testing = tf.summary.FileWriter(os.path.join(os.getcwd(),'/home/ksrivastava/DL/CIFAR10/scripts/logsTf/'+pid+'/validation'), sess.graph)
#-------------------------------------------------------------------------

sess.run(init)
start = time.time()
step=0
#data1=list()
#labels1=list()
print("Training started !!!!!")
for epoch in range(100):
    #np.random.shuffle(training)
    #for i in training:
    #    data1.append(i[0])
    #    labels1.append(i[1])
    for batch in range(n_batches):
        x_batch = training_data[batch*batch_size: (batch+1) * batch_size]
        y_batch = training_label[batch*batch_size: (batch+1) * batch_size]
        #train_step.run(feed_dict={x: x_batch, y_: y_batch, keep_prob: 0.5})
        sess.run(train_step,feed_dict={x: x_batch, y_: y_batch, keep_prob: 0.6} )
        if step%25 == 0:
            loss, acc = sess.run([cross_entropy, accuracy], feed_dict={x: training_data[:batch_size], y_: training_label[:batch_size], keep_prob: 1.0})
            summary = sess.run(merge_summary, feed_dict={x: training_data[:batch_size], y_: training_label[:batch_size], keep_prob: 1.0})
            summary_training.add_summary(summary, step)
            loss_val, acc_val = sess.run([cross_entropy, accuracy], feed_dict={x: validation_data[:batch_size], y_: validation_label[:batch_size], keep_prob: 1.0})
            summary_t = sess.run(merge_summary, feed_dict={x: validation_data[:batch_size], y_: validation_label[:batch_size], keep_prob: 1.0})
            summary_testing.add_summary(summary_t, step)
            #file.write(str(loss)+" ")
            #file.write(str(acc))
            #file.write("\n")
            print("step size " + str(step))
            print("Loss= " + "{:.6f}".format(loss) + ", Training Accuracy= " + "{:.5f}".format(acc))
            print("Validation Loss= " + "{:.6f}".format(loss_val) + ", Validation Accuracy= " + "{:.5f}".format(acc_val))
            misc.log_file("step size " + str(step), pid)
            misc.log_file("Loss= " + "{:.6f}".format(loss) + ", Training Accuracy= " + "{:.5f}".format(acc), pid)
            misc.log_file("Validation Loss= " + "{:.6f}".format(loss_val) + ", Validation Accuracy= " + "{:.5f}".format(acc_val), pid)
                #elapsed = time.time() - start
                #print(elapsed)
        step+=1
    #data1[:]=[]
    #labels1[:]=[]

#--------------------------------------- Save the trained model-----------------------------------------------------
misc.save_net(sess = sess, x = x, y_conv = y_conv, y_ = y_, cross_entropy = cross_entropy, accuracy=accuracy, filename="trainedNet", pid=pid)

#--------------------------------Test accuracy ----------------------------------------------
#print("Test accuracy")
#acc_test = sess.run(accuracy,feed_dict={x: test_data, y_: test_label, keep_prob: 1.0})
#print(acc_test)
#misc.log_file('Test accuracy: %f'%acc_test, pid)
#file.write("Test accuracy \n")
#file.write(str(acc))
#file.close()
sess.close()
print("Training ended !!!")
elapsed = time.time() - start
print(elapsed, "sec")
misc.log_file("Time elapsed: %d sec"%elapsed, pid)
